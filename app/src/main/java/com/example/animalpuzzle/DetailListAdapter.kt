package com.example.animalpuzzle

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Build
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView

class DetailListAdapter(bitmaps : ArrayList<PuzzleBitmap>, detailSideDimension : Int, dndPrc : DragAndDropProcessing)
    : RecyclerView.Adapter<DetailListAdapter.DetailListViewHolder>() {
    private val mBitmaps = bitmaps
    private val mDetailSideDimension : Int = detailSideDimension
    private val mDndPrc = dndPrc

    @SuppressLint("ClickableViewAccessibility")
    inner class DetailListViewHolder(item : View) : RecyclerView.ViewHolder(item) {}

    @SuppressLint("ResourceType")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : DetailListViewHolder {
        val detail1 = ImageView(parent.context)
        detail1.scaleType = ImageView.ScaleType.FIT_XY
        detail1.layoutParams = LinearLayout.LayoutParams(mDetailSideDimension, mDetailSideDimension)
        detail1.id = 1

        val detail2 = ImageView(parent.context)
        detail2.scaleType = detail1.scaleType
        detail2.layoutParams = detail1.layoutParams
        detail2.id = 2

        val row = LinearLayout(parent.context)
        row.layoutParams = LinearLayout.LayoutParams(mDetailSideDimension * 2, mDetailSideDimension)
        row.addView(detail1)
        row.addView(detail2)

        return DetailListViewHolder(row)
    }

    override fun getItemCount(): Int {
        return mBitmaps.size / 2 + mBitmaps.size % 2
    }

    @SuppressLint("ClickableViewAccessibility", "ResourceType")
    override fun onBindViewHolder(holder: DetailListViewHolder, position: Int) {
        val layout = holder.itemView as LinearLayout
        val leftDetail = layout.findViewById<ImageView>(1)
        val rightDetail = layout.findViewById<ImageView>(2)
        val leftIndex = position * 2
        val rightIndex = position * 2 + 1

        leftDetail.tag = mBitmaps[leftIndex].getIndex()
        leftDetail.setImageBitmap(Bitmap.createScaledBitmap(mBitmaps[leftIndex].getBitmap(), mDetailSideDimension, mDetailSideDimension, true))
        leftDetail.setOnTouchListener(mDndPrc.touchListener)
        leftDetail.visibility = View.VISIBLE

        if(rightIndex <= mBitmaps.size - 1) {
            rightDetail.tag = mBitmaps[rightIndex].getIndex()
            rightDetail.setImageBitmap(Bitmap.createScaledBitmap(mBitmaps[rightIndex].getBitmap(), mDetailSideDimension, mDetailSideDimension, true))
            rightDetail.setOnTouchListener(mDndPrc.touchListener)
            rightDetail.visibility = View.VISIBLE
        }
        else {
            rightDetail.setImageBitmap(null)
        }
    }

    fun removeItem(tag : Int) {
        var bmIndex = 0
        for(i in mBitmaps.indices) {
            val detail = mBitmaps[i]
            if(detail.getIndex() == tag) {
                bmIndex = i
                break
            }
        }

        mBitmaps.removeAt(bmIndex)
        notifyDataSetChanged()
    }
}