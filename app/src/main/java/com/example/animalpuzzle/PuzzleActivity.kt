package com.example.animalpuzzle

import android.annotation.SuppressLint
import android.app.Application
import android.graphics.*
import android.media.MediaPlayer
import android.os.Bundle
import android.view.WindowManager
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.doOnLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.animalpuzzle.databinding.ActivityPuzzleBinding
import kotlin.math.sqrt
import kotlinx.coroutines.*

class PuzzleActivity : AppCompatActivity() {
    private var mBitmaps = arrayListOf<PuzzleBitmap>()
    private var mRowsCount = 2
    private var mColumnsCount = 2
    private var mPlayer = MediaPlayer()
    private val mActivityContext = this
    private val mActivityCoroutineScope = CoroutineScope(Dispatchers.Default)
    private lateinit var binding: ActivityPuzzleBinding

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPuzzleBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        supportActionBar?.hide()

        val puzzlePartsNumber = intent.getIntExtra(PUZZLE_PARTS, 0)
        if(puzzlePartsNumber != 0) {
            mRowsCount = sqrt(puzzlePartsNumber.toDouble()).toInt()
            mColumnsCount = sqrt(puzzlePartsNumber.toDouble()).toInt()
        }

        mActivityCoroutineScope.launch {
            createBitmaps()
            withContext(Dispatchers.Main) {
                binding.Content.doOnLayout {
                    binding.relativeLayout.removeView(binding.slotsProgressBar)
                    val slotWidth = binding.relativeLayout.width/mColumnsCount
                    val slotHeight = binding.relativeLayout.height/mRowsCount

                    mPlayer = MediaPlayer.create(mActivityContext, intent.getIntExtra(SOUND_ID, 0))
                    val dndPrc = DragAndDropProcessing(binding, mPlayer, mRowsCount * mColumnsCount,
                        mBitmaps, Point((slotWidth * 1.4).toInt(), (slotHeight * 1.4).toInt()), mActivityCoroutineScope)

                    // Create slots
                    for(i in mBitmaps.indices.reversed()) { // We need reverse order to overlap unusable part of view
                        val bitmap = mBitmaps[i]
                        val slot = ImageView(applicationContext)
                        binding.relativeLayout.addView(slot)

                        val row : Int = i / mColumnsCount // rows number is mBitmaps.size() / mColumnsCount
                        val column : Int = i % mColumnsCount
                        val layoutParams = RelativeLayout.LayoutParams((slotWidth * 1.4).toInt(), (slotHeight * 1.4).toInt())
                        layoutParams.setMargins((-slotWidth * 0.4).toInt() + slotWidth * column, (-slotHeight * 0.4).toInt() + slotHeight * row, 0, 0)

                        slot.layoutParams = layoutParams
                        slot.scaleType = ImageView.ScaleType.FIT_XY
                        slot.tag = bitmap.getIndex()
                        slot.alpha = 0.3f
                        slot.setImageBitmap(Bitmap.createScaledBitmap(bitmap.getBitmap(), (slotWidth * 1.4).toInt(), (slotHeight * 1.4).toInt(), true))
                        slot.setOnDragListener(dndPrc.dragListener)
                    }

                    // Create details
                    mBitmaps.shuffle()
                    val detailList = findViewById<RecyclerView>(R.id.detailList)
                    detailList.layoutManager = LinearLayoutManager(Application())
                    detailList.adapter = DetailListAdapter(mBitmaps, detailList.width/2, dndPrc)
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mActivityCoroutineScope.cancel()
        mPlayer.stop()
        mPlayer.release()
    }

    private fun createBitmaps() {
        val backgroundImageId = intent.getIntExtra(IMAGE_ID, 0)
        val animalFullBm = BitmapFactory.decodeResource(resources, backgroundImageId)
        val animalPartBmWidth = animalFullBm.width/mColumnsCount
        val animalPartBmHeight = animalFullBm.height/mRowsCount

        val puzzleCreator = PuzzlePartCreator()
        var uniqueIndex = 1

        for(rowIndex in 1..mRowsCount) {
            for(columnIndex in 1..mColumnsCount) {

                // Set coordinates
                var x = ((columnIndex - 1.4) * animalPartBmWidth).toInt()
                if(x < 0) x = 0
                var y = ((rowIndex - 1.4) * animalPartBmHeight).toInt()
                if(y < 0) y = 0

                // Set dimensions
                var width = (animalPartBmWidth * 1.4).toInt()
                var height = (animalPartBmHeight * 1.4).toInt()
                if(rowIndex == 1)
                    height = animalPartBmHeight
                if(columnIndex == 1)
                    width = animalPartBmWidth

                // Set bumpType
                val bump = BumpType()
                if(columnIndex == 1) bump.left = true
                if(rowIndex == 1) bump.top = true
                if(columnIndex == mColumnsCount) bump.right = true
                if(rowIndex == mRowsCount) bump.bottom = true

                val bm = PuzzleBitmap(Bitmap.createBitmap(animalFullBm, x, y, width, height), uniqueIndex++, bump)
                mBitmaps.add(puzzleCreator.getPuzzleLikeForm(bm))
            }
        }
    }
}