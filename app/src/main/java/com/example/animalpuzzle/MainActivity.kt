package com.example.animalpuzzle
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.MobileAds

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        supportActionBar?.hide()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MobileAds.initialize(this) {}

        val bigRadioBtn = findViewById<Button>(R.id.btn_size_4)
        val mediumRadioBtn = findViewById<Button>(R.id.btn_size_9)
        val smallRadioBtn = findViewById<Button>(R.id.btn_size_16)

        bigRadioBtn.setOnClickListener {
            if(bigRadioBtn.isChecked) {
                mediumRadioBtn.isChecked = false
                smallRadioBtn.isChecked = false
            }
        }

        mediumRadioBtn.setOnClickListener {
            if(mediumRadioBtn.isChecked) {
                bigRadioBtn.isChecked = false
                smallRadioBtn.isChecked = false
            }
        }

        smallRadioBtn.setOnClickListener {
            if(smallRadioBtn.isChecked) {
                bigRadioBtn.isChecked = false
                mediumRadioBtn.isChecked = false
            }
        }

        val recyclerView = findViewById<RecyclerView>(R.id.animalList)
        recyclerView.adapter = AnimalListAdapter(this)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus)
            hideSystemUI()
    }

    private fun hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                // Set the content to appear under the system bars so that the
                // content doesn't resize when the system bars hide and show.
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)
    }
}