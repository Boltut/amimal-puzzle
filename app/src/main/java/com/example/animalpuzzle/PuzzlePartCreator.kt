package com.example.animalpuzzle

import android.graphics.*

class PuzzlePartCreator {

    private fun increaseDetail(pzBm: PuzzleBitmap) : Bitmap
    {
        val bmSrc = pzBm.getBitmap()
        val bumpType = pzBm.getBumpType()

        if(!bumpType.top && !bumpType.left)
            return bmSrc

        var newSrcBm  = Bitmap.createBitmap(bmSrc.width, bmSrc.height, Bitmap.Config.ARGB_8888)
        if(bumpType.left && bumpType.top) {
            newSrcBm = Bitmap.createBitmap((bmSrc.width * 1.4).toInt(), (bmSrc.height * 1.4).toInt(), Bitmap.Config.ARGB_8888)
            val canvas = Canvas(newSrcBm)
            val rect = Rect()
            rect.left = (bmSrc.width * 0.4).toInt()
            rect.top = (bmSrc.height * 0.4).toInt()
            rect.right = (bmSrc.width * 1.4).toInt()
            rect.bottom = (bmSrc.height * 1.4).toInt()
            canvas.drawBitmap(bmSrc, null, rect, null)
        }
        else if(bumpType.left) {
            newSrcBm = Bitmap.createBitmap((bmSrc.width * 1.4).toInt(), bmSrc.height, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(newSrcBm)
            val rect = Rect()
            rect.left = (bmSrc.width * 0.4).toInt()
            rect.top = 0
            rect.right = (bmSrc.width * 1.4).toInt()
            rect.bottom = bmSrc.height
            canvas.drawBitmap(bmSrc, null, rect, null)
        }
        else if(bumpType.top) {
            newSrcBm = Bitmap.createBitmap(bmSrc.width, (bmSrc.height * 1.4).toInt(), Bitmap.Config.ARGB_8888)
            val canvas = Canvas(newSrcBm)
            val rect = Rect()
            rect.left = 0
            rect.top = (bmSrc.height * 0.4).toInt()
            rect.right = bmSrc.width
            rect.bottom = (bmSrc.height * 1.4).toInt()
            canvas.drawBitmap(bmSrc, null, rect, null)
        }

        return newSrcBm
    }

    fun getPuzzleLikeForm(bmSrc: PuzzleBitmap) : PuzzleBitmap {
        val bumpType = bmSrc.getBumpType()
        val increasedBm : Bitmap = increaseDetail(bmSrc)

        val bmDst = Bitmap.createBitmap(increasedBm.width, increasedBm.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bmDst)

        val width = bmDst.width.toFloat()
        val height = bmDst.height.toFloat()

        val path = Path()
        path.reset()

        // Bottom right corner
        path.moveTo(width, height)
        path.lineTo(width, height * 5 / 7)

        // Right bump
        if(bumpType.right) {
            path.lineTo(width, height * 4 / 7)
        }
        else {
            path.cubicTo(width * 5 / 7, height * 6 / 7, width * 5 / 7, height * 3 / 7, width, height * 4 / 7)
        }

        // Top right corner
        path.lineTo(width, height * 2 / 7)
        path.lineTo(width * 5 / 7, height * 2 / 7)

        // Top bump
        if(bumpType.top) {
            path.lineTo(width * 4 / 7, height * 2 / 7)
        }
        else {
            path.cubicTo(width * 6 / 7, 0f, width * 3 / 7, 0f, width * 4 / 7, height * 2 / 7)
        }

        // Top left corner
        path.lineTo(width * 2 / 7, height * 2 / 7)
        path.lineTo(width * 2 / 7, height * 4 / 7)

        // Left bump
        if(bumpType.left) {
            path.lineTo(width * 2 / 7, height * 5 / 7)
        }
        else {
            path.cubicTo(0f, height * 3 / 7, 0f, height * 6 / 7, width * 2 / 7, height * 5 / 7)
        }

        // Bottom left corner
        path.lineTo(width * 2 / 7, height)
        path.lineTo(width * 4 / 7, height)

        // Bottom bump
        if(bumpType.bottom) {
            path.lineTo(width * 5 / 7, height)
        }
        else {
            path.cubicTo(width * 3 / 7, height * 5 / 7, width * 6 / 7, height * 5 / 7, width * 5 / 7, height)
        }

        path.lineTo(width, height)
        path.close()

        // Draw puzzle form
        val paint = Paint()
        paint.color = Color.DKGRAY
        paint.style = Paint.Style.FILL
        paint.isAntiAlias = true
        canvas.drawPath(path, paint)

        // Mask puzzle from
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        canvas.drawBitmap(increasedBm, 0f, 0f, paint)

        // Draw a white border
        var border = Paint()
//        border.color = Color.WHITE
//        border.style = Paint.Style.STROKE
//        border.strokeWidth = 10.0f
//        border.isAntiAlias = true
//        canvas.drawPath(path, border)

        // Draw a black border
        border = Paint()
        border.color = Color.BLACK
        border.style = Paint.Style.STROKE
        border.strokeWidth = 20.0f
        border.isAntiAlias = true
        canvas.drawPath(path, border)

        return PuzzleBitmap(bmDst, bmSrc.getIndex(), bumpType)
    }
}