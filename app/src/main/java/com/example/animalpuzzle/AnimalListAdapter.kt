package com.example.animalpuzzle

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.*
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback

const val IMAGE_ID = "com.example.animalpuzzle.IMAGE"
const val SOUND_ID = "com.example.animalpuzzle.SOUND"
const val PUZZLE_PARTS = "com.example.animalpuzzle.PUZZLE_PARTS"

class AnimalListAdapter(private val mActivity : Activity) : RecyclerView.Adapter<AnimalListAdapter.AnimalListViewHolder>() {
    private var mListHeight = 0
    private val mCoroutineScope = CoroutineScope(Dispatchers.Default)

    // Ad settings
    private val mInterstitialAdUnitId: String = "ca-app-pub-3940256099942544/1033173712" // testing value
    private var mInterstitialAd: InterstitialAd? = null
    private lateinit var mPuzzleActivityIntent : Intent
    private var mPuzzleStartTimes = 0 // Remember how much times we start puzzle activities. We show ad on every 4th puzzle.

    init {
        loadAd()
    }

    inner class AnimalListViewHolder(item : View) : RecyclerView.ViewHolder(item) {
        val animalView : ImageView = item.findViewById(R.id.animalListEntry)
        val animalName : TextView = item.findViewById(R.id.animalName)
        val progressBar : ProgressBar = item.findViewById(R.id.animalEntryProgressBar)
        val width = item.layoutParams.width
        val height = item.layoutParams.height
        var backgroundImageId : Int = 0
        var soundId : Int = 0

        init {
            animalView.setOnClickListener {
                var puzzlePartsCount = 0
                val bigRadioBtn = mActivity.findViewById<RadioButton>(R.id.btn_size_4)
                val mediumRadioBtn = mActivity.findViewById<RadioButton>(R.id.btn_size_9)
                val smallRadioBtn = mActivity.findViewById<RadioButton>(R.id.btn_size_16)

                if(bigRadioBtn.isChecked)
                    puzzlePartsCount = 4
                else if(mediumRadioBtn.isChecked)
                    puzzlePartsCount = 9
                else if(smallRadioBtn.isChecked)
                    puzzlePartsCount = 16

                val context = it.context
                mPuzzleActivityIntent = Intent(context, PuzzleActivity::class.java).apply {
                    putExtra(IMAGE_ID, backgroundImageId)
                    putExtra(SOUND_ID, soundId)
                    putExtra(PUZZLE_PARTS, puzzlePartsCount)
                }

                if(mPuzzleStartTimes < 3) {
                    mActivity.startActivity(mPuzzleActivityIntent)
                    ++mPuzzleStartTimes
                }
                else {
                    mPuzzleStartTimes = 0
                    showAd()
                }
            }
        }
    }

    override fun getItemCount(): Int = 23

    override fun onBindViewHolder(holder: AnimalListViewHolder, position: Int) {
        var scaledBitmapFromResources: Bitmap
        holder.animalView.alpha = 0f // Avoid cached background image while scrolling list
        holder.progressBar.alpha = 1f // Show progress bar while bitmap is processing

        when(position) {
            0 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.cat, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = mActivity.getString(R.string.cat)
                holder.backgroundImageId = R.drawable.cat
                holder.soundId = R.raw.cat
            }
            1 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.dog, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = mActivity.getString(R.string.dog)
                holder.backgroundImageId = R.drawable.dog
                holder.soundId = R.raw.dog
            }
            2 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.mouse, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = mActivity.getString(R.string.mouse)
                holder.backgroundImageId = R.drawable.mouse
                holder.soundId = R.raw.mouse
            }
            3 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.bear, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = mActivity.getString(R.string.bear)
                holder.backgroundImageId = R.drawable.bear
                holder.soundId = R.raw.bear
            }
            4 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.wolf, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = mActivity.getString(R.string.wolf)
                holder.backgroundImageId = R.drawable.wolf
                holder.soundId = R.raw.wolf
            }
            5 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.elephant, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = mActivity.getString(R.string.elephant)
                holder.backgroundImageId = R.drawable.elephant
                holder.soundId = R.raw.elephant
            }
            6 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.fox, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = mActivity.getString(R.string.fox)
                holder.backgroundImageId = R.drawable.fox
                holder.soundId = R.raw.fox
            }
            7 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.goose, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = mActivity.getString(R.string.goose)
                holder.backgroundImageId = R.drawable.goose
                holder.soundId = R.raw.goose
            }
            8 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.hawk, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = mActivity.getString(R.string.hawk)
                holder.backgroundImageId = R.drawable.hawk
                holder.soundId = R.raw.hawk
            }
            9 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.lamb, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = mActivity.getString(R.string.lamb)
                holder.backgroundImageId = R.drawable.lamb
                holder.soundId = R.raw.lamb
            }
            10 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.pig, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = mActivity.getString(R.string.pig)
                holder.backgroundImageId = R.drawable.pig
                holder.soundId = R.raw.pig
            }
            11 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.lion, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = mActivity.getString(R.string.lion)
                holder.backgroundImageId = R.drawable.lion
                holder.soundId = R.raw.lion
            }
            12 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.rooster, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = mActivity.getString(R.string.rooster)
                holder.backgroundImageId = R.drawable.rooster
                holder.soundId = R.raw.rooster
            }
            13 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.owl, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = "Owl"
                holder.backgroundImageId = R.drawable.owl
                holder.soundId = R.raw.owl
            }
            14 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.squirrel, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = "Squirrel"
                holder.backgroundImageId = R.drawable.squirrel
                holder.soundId = R.raw.squirrel
            }
            15 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.frog, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = "Frog"
                holder.backgroundImageId = R.drawable.frog
                holder.soundId = R.raw.frog
            }
            16 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.rhino, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = "Rhino"
                holder.backgroundImageId = R.drawable.rhino
                holder.soundId = R.raw.rhino
            }
            17 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.chimpanze, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = "Chimpanzee"
                holder.backgroundImageId = R.drawable.chimpanze
                holder.soundId = R.raw.chimpanzee
            }
            18 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.elk, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = "Elk"
                holder.backgroundImageId = R.drawable.elk
                holder.soundId = R.raw.elk
            }
            19 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.bison, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = "Bison"
                holder.backgroundImageId = R.drawable.bison
                holder.soundId = R.raw.bison
            }
            20 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.tiger, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = "Tiger"
                holder.backgroundImageId = R.drawable.tiger
                holder.soundId = R.raw.tiger
            }
            21 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.horse, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = "Horse"
                holder.backgroundImageId = R.drawable.horse
                holder.soundId = R.raw.horse
            }
            22 -> {
                mCoroutineScope.launch {
                    scaledBitmapFromResources = BitmapDecoder().run { decodeSampledBitmapFromResource(holder.animalView.context.resources, R.drawable.grasshopper, holder.width, holder.height) }
                    withContext(Dispatchers.Main) {
                        holder.animalView.setImageBitmap(Bitmap.createScaledBitmap(scaledBitmapFromResources, holder.width, holder.height, true))
                        holder.animalView.alpha = 1f
                        holder.progressBar.alpha = 0f
                    }
                }

                holder.animalName.text = "Grasshopper"
                holder.backgroundImageId = R.drawable.grasshopper
                holder.soundId = R.raw.grasshopper
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimalListViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        val entry = inflater.inflate(R.layout.animal_list_entry, parent, false)

        if(mListHeight == 0)
            mListHeight = parent.height

        entry.layoutParams = ViewGroup.LayoutParams((mListHeight * 0.9).toInt(), mListHeight)
        return AnimalListViewHolder(entry)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        mCoroutineScope.cancel()
    }

    private fun loadAd() {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(mActivity, mInterstitialAdUnitId, adRequest, object : InterstitialAdLoadCallback() {
            override fun onAdFailedToLoad(adError: LoadAdError) {
                //Log.d("PuzzleActivity", adError.message)
                mInterstitialAd = null
            }
            override fun onAdLoaded(interstitialAd: InterstitialAd) {
                //Log.d("PuzzleActivity", "Ad was loaded.")
                mInterstitialAd = interstitialAd
            }
        })
    }

    private fun showAd() {
        if (mInterstitialAd != null) {
            mInterstitialAd?.fullScreenContentCallback = object : FullScreenContentCallback() {
                override fun onAdDismissedFullScreenContent() {
                    //Log.d("PuzzleActivity", "Ad was dismissed.")
                    // Don't forget to set the ad reference to null so you
                    // don't show the ad a second time.
                    mInterstitialAd = null
                    loadAd()
                    mActivity.startActivity(mPuzzleActivityIntent)
                }

                override fun onAdFailedToShowFullScreenContent(p0: AdError) {
                    //Log.d("PuzzleActivity", "Ad failed to show.")
                    // Don't forget to set the ad reference to null so you
                    // don't show the ad a second time.
                    mInterstitialAd = null
                }

                override fun onAdShowedFullScreenContent() {
                    //Log.d("PuzzleActivity", "Ad showed fullscreen content.")
                }
            }
            mInterstitialAd?.show(mActivity)
        }
        else {
            mActivity.startActivity(mPuzzleActivityIntent)
        }
    }
}