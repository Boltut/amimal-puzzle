package com.example.animalpuzzle

import android.graphics.Bitmap

class BumpType {
    var left = false
    var top = false
    var right = false
    var bottom = false
}

class PuzzleBitmap(bm : Bitmap, index : Int, bump : BumpType) {
    private val mBm = bm
    private val mIndex = index // tag for correspondence between detail and slot while drag and drop
    private val mBump = bump

    fun getBitmap() : Bitmap = mBm
    fun getIndex() : Int = mIndex
    fun getBumpType() : BumpType = mBump
}