package com.example.animalpuzzle

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ClipData
import android.content.ClipDescription
import android.content.Context
import android.graphics.Canvas
import android.graphics.Point
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.media.MediaPlayer
import android.os.Build
import android.view.DragEvent
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.animalpuzzle.databinding.ActivityPuzzleBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.math.abs

class DragAndDropProcessing(puzzleBinding: ActivityPuzzleBinding, player: MediaPlayer, detailsCount : Int, bitmaps : ArrayList<PuzzleBitmap>, shadowSize : Point, coroutineScope : CoroutineScope) {
    private val mBitmaps = bitmaps
    private val mShadowSize = shadowSize
    private var mSuccessDropsCount : Int = 0
    private var mFirstElementIsVisible = false
    private var mLastElementIsVisible = false
    private var mIsMissDropHandled = false
    private lateinit var mDetailList : RecyclerView

    // Check does list scrolled fully to top or bottom
    // If yes - don't ignore vertical gesture
    private fun checkListFullyScrolled() {
        val detailListLM = mDetailList.layoutManager as LinearLayoutManager
        val first : Int = detailListLM.findFirstCompletelyVisibleItemPosition()
        val last = detailListLM.findLastCompletelyVisibleItemPosition()
        val rowsCount = mBitmaps.size/2  + mBitmaps.size % 2

        mFirstElementIsVisible = first == 0
        mLastElementIsVisible = last == rowsCount - 1
    }

    @SuppressLint("ResourceType")
    val dragListener = View.OnDragListener { v, event ->
        when (event.action) {
            DragEvent.ACTION_DRAG_STARTED -> {
                true
            }
            DragEvent.ACTION_DRAG_ENTERED -> {
                true
            }
            DragEvent.ACTION_DRAG_LOCATION -> {
                true
            }
            DragEvent.ACTION_DRAG_EXITED -> {
                true
            }
            DragEvent.ACTION_DROP -> {
                val item: ClipData.Item = event.clipData.getItemAt(0)
                val detailTag = item.text as String
                val slotTag = (v as? ImageView)?.tag.toString()

                if (slotTag == detailTag) {
                    (v as? ImageView)?.alpha = 1.0f
                    v.invalidate()

                    coroutineScope.launch {
                        withContext(Dispatchers.Main) {
                            val detailListAdapter = mDetailList.adapter as DetailListAdapter
                            detailListAdapter.removeItem(detailTag.toInt())

                            if(++mSuccessDropsCount == detailsCount) {
                                player.start()

                                val puzzleFinish = puzzleBinding.puzzleFinish
                                puzzleFinish?.alpha = 1.0f
                            }
                        }
                    }

                    true
                }
                else {
                    false
                }
            }

            DragEvent.ACTION_DRAG_ENDED -> {
                when(event.result) {
                    true -> {
                    }
                    false -> {
                        if(!mIsMissDropHandled) {
                            val itemCount = (mDetailList.adapter as DetailListAdapter).itemCount

                            // Handle miss drop - make visible all details
                            coroutineScope.launch {
                                withContext(Dispatchers.Main) {
                                    for(item in 0 .. itemCount) {
                                        val holder = mDetailList.findViewHolderForAdapterPosition(item)
                                        if(holder != null) {
                                            val leftDetail = holder.itemView.findViewById<ImageView>(1)
                                            val rightDetail = holder.itemView.findViewById<ImageView>(2)
                                            leftDetail?.visibility = View.VISIBLE
                                            rightDetail?.visibility = View.VISIBLE
                                        }
                                    }
                                }
                            }
                        }

                        mIsMissDropHandled = true
                    }
                }
                true
            }
            else -> {
                false
            }
        }
    }
    val touchListener = TouchListener(puzzleBinding)

    @RequiresApi(Build.VERSION_CODES.N)
    val longClickListener = View.OnLongClickListener {
        it.visibility = View.INVISIBLE
        val item = ClipData.Item(it.tag.toString(),"")
        val dragData = ClipData(item.text, arrayOf(ClipDescription.MIMETYPE_TEXT_PLAIN), item)
        val myShadow = View.DragShadowBuilder(it)

        // Starts the drag
        it.startDragAndDrop(dragData, myShadow, null, 0)
        true
    }

    inner class TouchListener(private val puzzleBinding: ActivityPuzzleBinding) : View.OnTouchListener {
        private var mTouchDownX = 0f
        private var mTouchDownY = 0f

        @RequiresApi(Build.VERSION_CODES.N)
        @SuppressLint("ClickableViewAccessibility")
        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            if (event != null) {
                when(event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        mTouchDownX = event.rawX
                        mTouchDownY = event.rawY
                        mDetailList = puzzleBinding.detailList
                        mIsMissDropHandled = false
                        checkListFullyScrolled()

                        return true
                    }
                    MotionEvent.ACTION_MOVE -> {
                        val moveX = event.rawX
                        val moveY = event.rawY
                        val deltaX = moveX - mTouchDownX
                        val deltaY = moveY - mTouchDownY

                        val startDragByVerticalGesture = (mFirstElementIsVisible && deltaY > 0)|| (mLastElementIsVisible && deltaY < 0)

                        if(abs(deltaX) > abs(deltaY) || startDragByVerticalGesture) { // Start drag only for horizontally gesture, avoiding vertical scroll
                            if (v != null) {
                                v.visibility = View.INVISIBLE
                                val item = ClipData.Item(v.tag.toString(),"")
                                val dragData = ClipData(item.text, arrayOf(ClipDescription.MIMETYPE_TEXT_PLAIN), item)
                                val myShadow = CustomDragShadowBuilder(v, v.tag.toString().toInt())
                                v.startDragAndDrop(dragData, myShadow, null, 0)
                            }
                            return true
                        }

                        return false
                    }
                    else -> {
                        return false
                    }
                }
            }
            return false
        }
    }

    inner class CustomDragShadowBuilder(view: View, tag : Int) : View.DragShadowBuilder(view) {
        private val mWidth = mShadowSize.x
        private val mHeight = mShadowSize.y
        private val mCurrentTag = tag
        private lateinit var mShadow : Drawable

        override fun onProvideShadowMetrics(outShadowSize: Point?, outShadowTouchPoint: Point?) {
            val currentBm = mBitmaps.find { it.getIndex() == mCurrentTag  }
            mShadow = BitmapDrawable(view.context.resources, currentBm?.getBitmap())
            mShadow.setBounds(0, 0, mShadowSize.x, mShadowSize.y)
            outShadowSize?.set(mWidth, mHeight)
            outShadowTouchPoint?.set((mWidth * 0.6).toInt(), (mHeight * 0.6).toInt())
        }

        override fun onDrawShadow(canvas: Canvas) {
            mShadow.draw(canvas)
        }
    }
}